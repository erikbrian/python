from django.urls import path
from .import views

urlpatterns =[
    path('', views.index, name='index'),
    path('proyectos/', views.proyectos, name='proyectos'),
    path('proyectos/<int:id>/', views.proyecto, name='proyecto'),
    path('proyectos/crear', views.form_proyectos_crear, name='form_proyectos_crear'),
    path('proyectos/crea_post', views.post_crear_proyecto, name='post_crear_proyecto'),
    path('actividades/', views.actividades, name='actividades')
]