from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.urls import reverse
from .models import Proyecto, Actividad

def index(request):
    return HttpResponse('Hola mundo')

def proyectos(request):

    proyectos_lista= Proyecto.objects.all()
    print(proyectos_lista)

    contexto= {
        
        'proyectos': proyectos_lista
        
    }

    return render(request, 'app/proyectos.html',contexto)  

def actividades(request):

    contexto= {
        'proyecto':'proyecto 23',
        'actividades': 10,
        'ciudades': ['roma','paris','londres', 'madrid'],
        'actividades':[
            {'id': 1, 'actvidad': 'Ver el modulo 3'},
            {'id': 2, 'nombre': 'Ver el modulo 4'}

        ]
    }

    return render(request, 'app/actividades.html',contexto)  

def form_proyectos_crear(request):
    return render(request, 'app/form_proyectos_crear.html')

def post_crear_proyecto(request):
    #Obtiene los datos del formulario
    nombre_param = request.POST['nombre']
    responsable_param = request.POST['responsable']

    #Crear el proyecto en la base de datos
    p=Proyecto(nombre=nombre_param, responsable=responsable_param)
    p.save()

    #redireciona a la pagina de proyectos
    return HttpResponseRedirect(reverse('proyectos'))

def proyecto(request, id):

    p = Proyecto.objects.get(pk=id)
    contexto={
        'proyecto':p
    }

    return render(request, 'app/proyecto.html', contexto)